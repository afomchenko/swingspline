package ru.anton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.CubicCurve2D;

/**
 * Created by Ailes on 29.07.2014.
 */
public class SwingSplineTest {

    MyDrawPanel drawPanel;
    Point[] contPoints;
    Point[] points;
    int currPoint;
    int part = 100;
    double t[];
    boolean isCreating;

    public static void main(String[] args) {
        SwingSplineTest sstest = new SwingSplineTest();
        sstest.go();
    }

    public void go() {
        JFrame frame = new JFrame("Spline test");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        drawPanel = new MyDrawPanel();
        drawPanel.setBounds(0, 0, 500, 500);
        drawPanel.addMouseListener(new MyMouseEvent());
        drawPanel.addMouseMotionListener(new MyMouseMotionEvent());
        contPoints = new Point[4];
        for (int i = 0; i < 4; i++) contPoints[i] = new Point();
        isCreating = true;


        frame.getContentPane().add(drawPanel);
        frame.setSize(500, 500);
        frame.setVisible(true);

    }

    class MyDrawPanel extends JPanel {

        @Override
        protected void paintComponent(Graphics g) {
            g.clearRect(0,0,500,500);
            Graphics2D g2d = (Graphics2D) g;
            g.setColor(Color.BLUE);


            for (Point pnt : contPoints)
                g.fillOval(pnt.x - 3, pnt.y - 3, 6, 6);

           if (currPoint == 0) {
                CubicCurve2D.Double curv = new CubicCurve2D.Double(contPoints[0].getX(), contPoints[0].getY(),
                        contPoints[1].getX(), contPoints[1].getY(),
                        contPoints[2].getX(), contPoints[2].getY(),
                        contPoints[3].getX(), contPoints[3].getY()
                );
                g2d.draw(curv);

               g.setColor(Color.red);

               setPoints();
               for(Point p: points){
                   g.drawLine(p.x, p.y, p.x, p.y);
               }


               g.drawLine(contPoints[0].x, contPoints[0].y,
                       contPoints[1].x, contPoints[1].y);
               g.drawLine(contPoints[2].x, contPoints[2].y,
                       contPoints[3].x, contPoints[3].y);

               isCreating = false;
            }



        }


    public void setPoints()
    {
        t = new double [part+1];
        double alfa = 1.0 / (double)part;
        for (int i=0; i<= part; i++)
            t[i] = 0 + i*alfa;
        points = new Point[t.length];


        Point tmp[] = new Point[contPoints.length];

        for (int i=0; i<points.length; i++)
        {

            for (int k=0; k<contPoints.length; k++)
                tmp[k] = new Point( contPoints[k].x, contPoints[k].y);

            int n = contPoints.length;
            while (n > 0)
            {
                for (int k=0; k < (n-1) ;k++)
                {
                    tmp[k].x = (int) (tmp[k].x + t[i]*(tmp[k+1].x - tmp[k].x));
                    tmp[k].y = (int) (tmp[k].y + t[i]*(tmp[k+1].y - tmp[k].y));
                }
                n--;
            }

            points[i] = new Point(tmp[0].x, tmp[0].y);
        }

    }

}

    class MyMouseEvent implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            if(!isCreating) {
                for (Point contPoint : contPoints) {
                    contPoint.setLocation(0, 0);
                }
                isCreating = true;
                currPoint = 0;
            }


            contPoints[currPoint].setLocation(e.getX(), e.getY());
            drawPanel.repaint();

            if (++currPoint > 3) currPoint = 0;
        }

        @Override
        public void mousePressed(MouseEvent e) {


        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }

    class MyMouseMotionEvent implements MouseMotionListener {

        @Override
        public void mouseDragged(MouseEvent e) {

            int j=searchPoint(e);
            if(j>=0 && j<=contPoints.length)
            contPoints[j].setLocation(e.getX(), e.getY());


            drawPanel.repaint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
        }

        private int searchPoint(MouseEvent e){
            for(int i=0; i<4; ++i)
            {
                if(contPoints[i].x < e.getX()+8 && contPoints[i].x > e.getX()-8 &&
                contPoints[i].y < e.getY()+8 && contPoints[i].y > e.getY()-8) return i;

            }
            return -1;
        }
    }


}
